var EventObservable = function() {
    this.handlers = {};
}
 
EventObservable.prototype = {
    subscribe: function(eventName, object, handler){
       if(typeof(this.handlers[eventName])=="undefined")
           this.handlers[eventName]=[]; 
       this.handlers[eventName].push([object, handler]);
    },

    fire: function(eventName, target, data){
        if(this.handlers[eventName]){
            for(var i = 0; i < this.handlers[eventName].length; i++){
                if( this.handlers[eventName][i][0] == target ) {
                    var handlerPair = this.handlers[eventName][i];
                    handlerPair[1](handlerPair[0], data);
                }
            }
        }       
    }
};
 
