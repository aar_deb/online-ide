var Editor = {
	/*
		This class is the line controlller and factory of the editor
	*/

	container: undefined,
	evob: undefined,

	make: function(data) {
		var textRenderer = $('#text-render')[0];
		for(var x=0; x<=data.length-1; x++) {

			var linFactory = new LineFactory(this, this.evob);
			textRenderer.appendChild(linFactory.makeLine(x));

			$('#line-renderer-' + x)[0].innerHTML = data[x];
			(new CodeAnalayzer($('#line-renderer-' + x)[0]));
		}
		
		$('#text-render').children().first().focus();
		//this.refreshLines();

	},

	getLineById: function(id) {
		for(var i =0; i<=this.container.children.length-1; i++) {
			if(this.container.children[i].id.substring(14,15) == id) {
				return this.container.children[i];
			}
		}
		return null;
	}

}

