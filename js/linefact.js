function LineFactory(controller, eventObserver) {
	this.controller = controller;
	this.evob = eventObserver;

	this.bindEvent = function(element, type, handler) {
	   if(element.addEventListener) {
	      element.addEventListener(type, handler, false);
	   } else {
	      element.attachEvent('on'+type, handler);
	   }
	};

	this.createLineElement = function(lineNo) {
		var newLineRend = document.createElement('section');
		newLineRend.id = 'line-renderer-' + lineNo;
		newLineRend.className = 'liner';
		newLineRend.contentEditable = 'true';
		
		return newLineRend;	
	};

	this.getOnFocusHandler = function(me, args) {
		me.style['outline'] = 'none';
		currentLine = parseInt(me.id.substring(14,15));
		me.style['background-color'] = 'rgb(56, 56, 56)';
	};

	this.getOnBlurHandler = function(me, args) {
		me.style['background-color'] = '#1d1d1d';
	};

	this.getOnKeyUpHandler = function(me, args) {
		var keycode = (args.keyCode ? args.keyCode : args.which);

		//do not allow up/down arrow events to run codeanalysis..
		if(keycode != 40 && keycode != 38)
			var cc = new CodeAnalayzer(me);
			//let only [a-z][A-Z] to make code completion *NEEDS UPDATING
			if(keycode >= 65 && keycode <= 90){
				cc.runCodeCompletion(args, me);					
		}
	};

	this.getOnContentChangeHandler = function(me, args) {
		(new CodeAnalayzer(me));
	};

	this.getOnKeyDownHandler = function(me, args) {
		var keycode = (args.keyCode ? args.keyCode : args.which);
		newLineRend = me;
		if(keycode == '13') //Enter
		{					
			args.preventDefault();

			if(isCompletorRunning()) {
				var childs = $('.dropdownmenu').children();
				for(var c=0; c<=childs.length-1;c++) {
					if(childs[c]['active'] == 'true') {
						childs[c].dispatchEvent(new Event('click'));
					}
				}

			}else {
				//BUG HERE
				var temp = createLineRenderer($("#text-render").get(0).childElementCount);
				$(temp).insertAfter(newLineRend);
				$(temp).focus();
				refreshLines();
				return false;
			}
		}
		else if(keycode == '8') //Backspace
		{
			//NEEDS MORE WORK
			if($(newLineRend).is(':empty') && $(newLineRend).index() != 0)
			{
				$('#line-renderer-' + (lineNo)).prev().focus();
				newLineRend.remove();
				refreshLines();
				return false;
			}
		}
		else if(keycode == '9') //Tab
		{
			var range = window.getSelection().getRangeAt(0);
			var preSelectionRange = range.cloneRange();
			preSelectionRange.selectNodeContents(newLineRend);
			preSelectionRange.setEnd(range.startContainer, range.startOffset);
			var start = preSelectionRange.toString().length;
			var savedRang = {
		   		start: start + 4,
		  		end: start + range.toString().length + 4
			};

			var text = $(newLineRend).text();
			if(text[start])
			{
				text = text.split('');
				text[start] = "\xa0\xa0\xa0\xa0" + text[start];
				text = text.join("");	
			}
			else
			{
				text += "\xa0\xa0\xa0\xa0 ";
			}
			$(newLineRend).text(text);
			restoreSelection(newLineRend, savedRang);
			return false;
		}
		else if(keycode == '38') //Up Arrow
		{
			args.preventDefault();

			if(isCompletorRunning()) {
				var childs = $('.dropdownmenu').children();

				for(var c=0; c<=childs.length-1;c++) {
					if(childs[c]['active'] == 'true')
					{
						//Is there another element above?
						if(c==0){
							//if there is no upward row, remove code completion
							removeCodeCompletor();
							return;
						}

						//Found active element
						//Set the current clause as false
						childs[c]['active'] = 'false';
						childs[c].style['background-color'] = '#FFF';
						//Set the new row as true;
						childs[c-1]['active'] = 'true';
						childs[c-1].style['background-color'] = 'rgba(201, 201, 201, 0.28)';
						return;
					}
				}

			} else {
				if($(newLineRend).index() != 0)
				{
					e = $('#line-renderer-' + (lineNo)).prev();
					
					var savedRang = getSelection(e[0]);

					restoreSelection(e[0], savedRang);
					//e.focus();
				}
			}
		}
		else if(keycode == '40') //Down Arrow
		{
			args.preventDefault();

			if(isCompletorRunning()) {
				var childs = $('.dropdownmenu').children();

				for(var c=0; c<=childs.length-1;c++) {
					if(childs[c]['active'] == 'true')
					{

						//Is there another element below?
						if( (c+1)==childs.length ){
							//if there is no upward row, remove code completion
							removeCodeCompletor();
							return;
						}

						//Found active element
						//Set the current clause as false
						childs[c]['active'] = 'false';
						childs[c].style['background-color'] = '#FFF';
						//Set the new row as true;
						childs[c+1]['active'] = 'true';
						childs[c+1].style['background-color'] = 'rgba(201, 201, 201, 0.28)';
						return;
					}
				}

			} else {
				if($(newLineRend).index() != $("#text-render").get(0).childElementCount)
				{
					$('#line-renderer-' + (lineNo)).next().focus();
				}
			}
		}
		else if(newLineRend.offsetHeight < newLineRend.scrollHeight)
		{
			return false;
		}
	};

	this.getSelection = function(elem) {
		try {
			var range = window.getSelection().getRangeAt(0);
			var preSelectionRange = range.cloneRange();
			preSelectionRange.selectNodeContents(elem);
			preSelectionRange.setEnd(range.startContainer, range.startOffset);
			var start = preSelectionRange.toString().length;
			return { start: start-1, end: start -1 + range.toString().length };
		}catch(Exception) {
			//console.log(Exception);
		}
	};

	this.restoreSelection = function(containerEl, savedSel) {
		var charIndex = 0, range = document.createRange();
		range.setStart(containerEl, 0);
		range.collapse(true);
		var nodeStack = [containerEl], node, foundStart = false, stop = false;

		while (!stop && (node = nodeStack.pop())) {
		    if (node.nodeType == 3) {
		        var nextCharIndex = charIndex + node.length;
		        if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
		            range.setStart(node, savedSel.start - charIndex);
		            foundStart = true;
		        }
		        if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
		            range.setEnd(node, savedSel.end - charIndex);
		            stop = true;
		        }
		        charIndex = nextCharIndex;
		    } else {
		        var i = node.childNodes.length;
		        while (i--) {
		            nodeStack.push(node.childNodes[i]);
		        }
		    }
		}

		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	};
};

LineFactory.prototype.makeLine = function(lineNo)	{
	//CREATE NEW LINE RENDER
	var newLineRend = this.createLineElement(lineNo);

	//Create line and add the value to it
	if(this.controller.getLineById(lineNo) == null) //Check if line exists
	{
		
		//register all events needed
		this.evob.subscribe("focus", newLineRend, this.getOnFocusHandler);
		this.evob.subscribe("blur", newLineRend, this.getOnBlurHandler);
		this.evob.subscribe("keyup", newLineRend, this.getOnKeyUpHandler);
		this.evob.subscribe("contentchange", newLineRend, this.getOnContentChangeHandler);
		this.evob.subscribe("keydown", newLineRend, this.getOnKeyDownHandler);

		//set an observer for each event
		newLineRend.onfocus = function(event) {
			globObserv.fire('focus', this, event);
		};

		newLineRend.onblur = function(event) {
			globObserv.fire('blur', this, event);
		};

		newLineRend.onkeyup = function(event) {
			globObserv.fire('keyup', this, event);
		};

		this.bindEvent(newLineRend, 'contentChange', function() {
			globObserv.fire('contentchange', this, null);
		});

		newLineRend.onkeydown = function(event)	{
			globObserv.fire('keydown', this, event);
		};

		
	} 
	return newLineRend;
};