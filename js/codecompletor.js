var CodeCompletor =  function(ev, elem) {

	init(ev, elem);

	function init(ev, elem) {
		//removeNoisySpaces(elem);
		try {
			var range = window.getSelection().getRangeAt(0);
			var preSelectionRange = range.cloneRange();
			preSelectionRange.selectNodeContents(elem);
			preSelectionRange.setEnd(range.startContainer, range.startOffset);
			var start = preSelectionRange.toString().length;
			var savedRang = {
			    start: start,
			    end: start + range.toString().length
			};
		}catch(Exception) {
			//console.log(Exception);
		}

		var text = elem.textContent;
		var l = 0;

		var wordPiece = '';


		for(var w = (start-1); w >= l; w--) {
			if(text[w].charCodeAt(0) == 160 || text[w].charCodeAt(0) == 46 || text[w].charCodeAt(0) == 32) {
				l = w;
				break;
			} else {
				wordPiece += text[w];
			}
		}

		var str = invertString(wordPiece);
		var wordMatchs = findWordsStartingWith(str);

		if(wordMatchs.length > 0) {

			var body = $('#file-drag')[0];
			var sens = fabricateIntellisensor(wordMatchs);
			sens.style['top'] = elem.offsetTop + 20 + 'px';
			sens.style['left'] = elem.offsetLeft + savedRang.start * 5 + 'px';

			body.appendChild(sens);

			
		}

		try {
			restoreSelection(elem, savedRang);
		}catch(Exception) {
			//console.log(Exception);
		}

		//addSpaces(elem);
	}

	function fabricateIntellisensor(wrds) {
		/*
		<ul class='dropdownmenu'>
			<li>
				<a tabindex="-1" href="#">word</a>
			</li>
		</ul>
		*/

		wrds.sort();

		var ulContainer = document.createElement('ul');
		ulContainer.className = 'dropdownmenu';

		for(var w =0; w <= wrds.length-1; w++) {
			var liWord = document.createElement('li');
			var a = document.createElement('a');

			
			a.innerHTML = boldOutSelection(wrds[w]['word'], 0, wrds[w]['wordsMatched']);
			liWord.appendChild(a);

			if(w==0) {
				liWord['active'] = 'true';
				liWord.style['background-color'] = 'rgba(201, 201, 201, 0.28)';
			} else {
				liWord['active'] = 'false';
				liWord.style['background-color'] = '#FFF';
			}

			liWord.id = w;
			
			liWord.onclick = function() { 
				var currID = this.id;
				var pluckedWord = {word: wrds[currID]['word'], wordsMatched: wrds[currID]['wordsMatched']};
				var wrd = pluckedWord['word'];
				var mwrdl = pluckedWord['wordsMatched'];
				insertThisWord(wrd, mwrdl); 
			};

			ulContainer.appendChild(liWord);
		}

		ulContainer.contentEditable = false;
		return ulContainer;
	}

	function insertThisWord(word, lenToStartFrom) {
		var currDiv = $('#line-renderer-' + currentLine)[0];
		var s = getSelection(currDiv);

		var continuedWord = '';
		for(var c=lenToStartFrom; c<=word.length-1; c++) {
			continuedWord += word[c];
		}

		var txt = currDiv.textContent;

		currDiv.textContent = txt.substring(0, s.start) + continuedWord + txt.substring(s.start, txt.length);

		currDiv.dispatchEvent(new Event('contentChange'));

		restoreSelection(currDiv, s);

		//Finally remove the completor
		removeCodeCompletor();
	}

	function boldOutSelection(wrd, start, end) {
		var bldWrd = '';
		for(var w = 0; w<=wrd.length-1; w++) {
			if(w == start) {
				bldWrd += '<b>';
			} else if(w == end) {
				bldWrd += '</b>';
			}
			bldWrd += wrd[w];
		}
		return bldWrd;
	}

	function findWordsStartingWith(stWrd) {
		var mts = [];
		for(var wrd =0; wrd <= cmvReturns.length-1; wrd++) {
			var wrdMths = 0;
			var stop = true;
			var valid = true;
			for(var x =0; x <= cmvReturns[wrd].length-1; x++) {
				if(cmvReturns[wrd][x] == stWrd[x] && stop) {
					wrdMths +=1;
				} else {
					stop = false;
					if(stWrd.length > wrdMths)
						valid = false;
				}
			}
			if(wrdMths > 0 && valid )  
				mts.push( {word: cmvReturns[wrd], wordsMatched: wrdMths} );
		}
		return mts;
	}


	function invertString(str) {
		var iStr = '';
		for(var v = str.length-1; v >=0; v--) {
			iStr += str[v];
		}
		return iStr;
	}

	function getSelection(elem) {
		try {
			var range = window.getSelection().getRangeAt(0);
			var preSelectionRange = range.cloneRange();
			preSelectionRange.selectNodeContents(elem);
			preSelectionRange.setEnd(range.startContainer, range.startOffset);
			var start = preSelectionRange.toString().length;
			return { start: start, end: start + range.toString().length };
		}catch(Exception) {
			console.log(Exception);
		}
	}

	function restoreSelection(containerEl, savedSel) {
		var charIndex = 0, range = document.createRange();
		range.setStart(containerEl, 0);
		range.collapse(true);
		var nodeStack = [containerEl], node, foundStart = false, stop = false;

		while (!stop && (node = nodeStack.pop())) {
		    if (node.nodeType == 3) {
		        var nextCharIndex = charIndex + node.length;
		        if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
		            range.setStart(node, savedSel.start - charIndex);
		            foundStart = true;
		        }
		        if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
		            range.setEnd(node, savedSel.end - charIndex);
		            stop = true;
		        }
		        charIndex = nextCharIndex;
		    } else {
		        var i = node.childNodes.length;
		        while (i--) {
		            nodeStack.push(node.childNodes[i]);
		        }
		    }
		}

		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	}

};