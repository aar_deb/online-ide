//Strict patterns
var detectPattern = [{toDetect: "function", regPattern: "(public|private|internal|protected).*?(static|abstract|virtual|unsafe|override).*?(returnVal).*?((?:[a-z][a-z]+))", valueToReplace: "returnVal"}];
var classReturns = "string|integer|float|decimal|double";

var cmvReturns = [ "string", "integer", "float", "decimal", "double", "public" ];


var CodeAnalayzer = function(div) {
	removeCodeCompletor();
	analyse(div);

	function analyse(div) {
		//get ranges
		try {
			var range = window.getSelection().getRangeAt(0);
			var preSelectionRange = range.cloneRange();
			preSelectionRange.selectNodeContents(div);
			preSelectionRange.setEnd(range.startContainer, range.startOffset);
			var start = preSelectionRange.toString().length;
			//console.log(start);
			var savedRang = {
			    start: start,
			    end: start + range.toString().length
			};
		}catch(Exception) {
			//console.log(Exception);
		}

		var text = div.textContent;
		//this.processedText = text;

		var matchs = initCodeParser(text);
		for(var x=0; x<=matchs.length-1; x++) {
			findAndLog(text, matchs[x]['patsFound'], matchs[x]['detected']);		
		}

		
		div.innerHTML = SyntaxHighlighter(text);
		
		try {
			restoreSelection(div, savedRang);
		}catch(Exception) {
			//console.log(Exception);
		}

		//addSpaces(div);
	};


	function findAndLog(text, match, toDetect) {
		f = function(mt) {
			if(mt != null) {
				var pt = "";

				for(var m=4; m<=mt.length-1 ;m++) {
					pt += mt[m];
					if(m < mt.length-1) {
						pt += "\xa0";
					}
				}
				this.processedText = text.replace(mt[0], pt);
			}
		};

		switch(toDetect) 
		{
			case "function":
				f(match);
				break;
		};

	};

	function getSelection(elem) {
			try {
			var range = window.getSelection().getRangeAt(0);
			var preSelectionRange = range.cloneRange();
			preSelectionRange.selectNodeContents(elem);
			preSelectionRange.setEnd(range.startContainer, range.startOffset);
			var start = preSelectionRange.toString().length;
			return { start: start-1, end: start -1 + range.toString().length };
		}catch(Exception) {
			//console.log(Exception);
		}
	}
	
	function initCodeParser(text) {
		var results = [];
		for(var d=0; d<= detectPattern.length-1; d++) {
			var pat = detectPattern[d]['regPattern'];
			pat = pat.replace("returnVal", classReturns);
			var regObj = new RegExp(pat, "i");
			var matches = regObj.exec(text); //text.match(regObj);

			if(matches != null) {
				results.push( { detected: detectPattern[d]['toDetect'] , patsFound: matches } );
			}
		}
		return results;
	}

	CodeAnalayzer.prototype.runCodeCompletion = function(ev, elem) {
		(new CodeCompletor(ev, elem));
	}

	function getHTMLPosFromTextPos(elem, actPos) {
		var cNodes = elem.childNodes;
		var lenArray = [];

		for(var ns= 0; ns <= cNodes.length-1; ns++) {
		    lenArray.push(cNodes[ns].textContent.length);
		}

		var totalLen = 0;
		var foundELEM;
		for(var l= 0; l <= lenArray.length-1; l++) {
			if(foundELEM == undefined) {
		    	totalLen += lenArray[l];
			    for(var n=0; n<= totalLen; n++) {
				    if(actPos == n){
				        foundELEM = l;
				        break;
				    }
				}
			}
		}

		//resize actual pos
		for(var u =0; u<=foundELEM-1; u++) {
			actPos = actPos - lenArray[u];
		}

		var elemHTML = cNodes[foundELEM];
		var elemText = '';
		if(elemHTML.nodeName != '#text') {
		    elemText = cNodes[foundELEM].innerHTML;
		} else {
			elemText = cNodes[foundELEM].data;
		}

		var strUNTIL = '';
		for(var w =0; w<=actPos-1; w++) {
		    strUNTIL += elemText[w];
		}

		var CURRENT_NODE_POS =0;
		if(elemHTML.nodeName != '#text') {
			for(var w=0; w<= elemHTML.outerHTML.length-1; w++) {
				if(elemHTML.outerHTML[w] == '>') {
					CURRENT_NODE_POS++;
					CURRENT_NODE_POS += strUNTIL.length-1;
					break;
				}
				CURRENT_NODE_POS++;
			}
		} else {
			CURRENT_NODE_POS = strUNTIL.length;
		}
		
		for(var y =0; y<=foundELEM-1; y++) {
			if(cNodes[y].nodeName != '#text') {
				CURRENT_NODE_POS += cNodes[y].outerHTML.length-1;
			} else {
				CURRENT_NODE_POS += cNodes[y].data.length-1;
			}
		}

		return CURRENT_NODE_POS;
	};

	function restoreSelection(containerEl, savedSel) {
		var charIndex = 0, range = document.createRange();
		range.setStart(containerEl, 0);
		range.collapse(true);
		var nodeStack = [containerEl], node, foundStart = false, stop = false;

		while (!stop && (node = nodeStack.pop())) {
		    if (node.nodeType == 3) {
		        var nextCharIndex = charIndex + node.length;
		        if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
		            range.setStart(node, savedSel.start - charIndex);
		            foundStart = true;
		        }
		        if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
		            range.setEnd(node, savedSel.end - charIndex);
		            stop = true;
		        }
		        charIndex = nextCharIndex;
		    } else {
		        var i = node.childNodes.length;
		        while (i--) {
		            nodeStack.push(node.childNodes[i]);
		        }
		    }
		}

		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	}

};